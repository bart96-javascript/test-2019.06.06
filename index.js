
/**
 * Задание 1.
 * Функция суммирует передаваемые в аргументе числа.
 * Если аргумент не передали, то должна вывести результат суммирования
 * @param {number} n - Число, которое мы хотим добавить к общей сумме
 * @return {number} Сумма всех переданных чисел.
 */
function sum(n) {
    let currentSum = n;
    let func = (nextInt) => nextInt == undefined ? currentSum : (currentSum += nextInt, func);

    return func;
}

console.log(sum(2)(3)(6)()); // 11



/**
 * Задание 2.
 * Функция возвращает объект, прототипом которого является другой объект, переданный в аргументе
 * @param {object} obj - Объект для прототипа.
 * @return {object} Новый объект в прототипе которого переданный объект.
 */
function createInheritObject(obj) {
    function MyObj() {};

    let myObj = new MyObj();
    myObj.prototype = obj;

    return myObj;
}

function Employee() {}

createInheritObject(new Employee());



/**
 * Задание 3.
 * Функция принимает массив из URL адресов, параллельно делаeт http запрос на каждый и возвращает Promise.
 * В обработчик then должен попадать массив с результатами от каждого запроса
 * @async
 * @param {Array} arr - Массив из URL адресов.
 * @return {Promise} Массив и результатами запросов.
 */
function multipleRequest(arr) {
    let req = arr.map(url => {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);

            xhr.onload = function() {
                if (this.status == 200) resolve(this.response);
                else {
                    var error = new Error(this.statusText);
                    error.code = this.status;
                    reject(error);
                }
            }

            xhr.onerror = function() {
                reject(new Error("Network Error"));
            };

            xhr.send();
        });
    });

    return Promise.all(req);
}

const arr = ['http://test1.com', 'http://test2.com', 'http://test3.com'];

multipleRequest(arr).then((results) => {
    console.log(results); // ['result1', 'result2', 'result3']
});



/**
 * Задание 4.
 * Функция проверяет правильность расстановки скобок (в математическом смысле) в переданной строке
 * Каждая открытая скобка должна быть закрыта в правильном порядке
 * @param {string} str - Строка из скобок, которую мы хотим проверить.
 * @return {boolean} true - если скобки расставлены правильно, false - если неправильно.
 */
function testBrackets(str) {
    // https://github.com/bart96-b/node-style-cli/blob/master/src/styleCli.js#L16
}

console.log(
    testBrackets('()'),       // true
    testBrackets('(()(()))'), // true
    testBrackets(')('),       // false
    testBrackets('(()()'),    // false
    testBrackets('())(()')    // false
);
